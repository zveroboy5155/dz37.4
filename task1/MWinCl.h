//
// Created by dor-g on 25.04.2023.
//
#include <QMainWindow>
#include <iostream>
#include <string>
#include <exception>
#include <QtWidgets/QLineEdit>
#ifndef DZ37_4_V2_MWINCL_H
#define DZ37_4_V2_MWINCL_H

//template<typename int>

class MwinnCall : public QMainWindow{
 Q_OBJECT
    void print_res(const QString &res){
        result->setText(res);
    }

    bool get_args(int &inp1, int &inp2){

        if (this->arg1->text().isEmpty())
        {
            print_res("ERROR: Аргумент 1 не введён");
            return false;
        }

        inp1 = this->arg1->text().toInt();
        if(!inp1 && this->arg1->text() != "0"){
            print_res("ERROR: Аргумент 1 (" + this->arg2->text() + ") не число");
            return false;
        }
        if (this->arg2->text().isEmpty())
        {
            print_res("ERROR: Аргумент 2 не введён");
            return false;
        }

        inp2 = this->arg2->text().toInt();
        if(!inp2 && this->arg2->text() != "0")
        {
            print_res("ERROR: Аргумент 2 (" + this->arg2->text() + ") не число");
            return false;
        }

        return true;
    }
public:
    QLineEdit *arg1 = nullptr;
    QLineEdit *arg2 = nullptr;
    QLineEdit *result = nullptr;
    MwinnCall(QWidget *parent = nullptr): QMainWindow(parent){};
public slots:
    void add(){
        int inp1, inp2;

        if(get_args(inp1, inp2))
            print_res(QString::number(inp1 + inp2));

    }
    void subt(){
        int inp1, inp2;

        if(get_args(inp1, inp2))
            print_res(QString::number(inp1 - inp2));
    }

    void multip(){
        int inp1, inp2;

        if(get_args(inp1, inp2))
            print_res(QString::number(inp1 * inp2));
    }

    void divis(){
        int inp1, inp2;

        if(get_args(inp1, inp2) && inp2 != 0.0f)
            print_res(QString::number(inp1 / inp2));

        if (this->result->text().isEmpty()) print_res("На ноль делить нельзя!!");
    }
};





#endif //DZ37_4_V2_MWINCL_H
