#include <QApplication>
#include <QPushButton>
#include "./MWinCl.h"
#include "./ui_DesQt1.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    MwinnCall window(nullptr);
    Ui::MainWindow caller;
    caller.setupUi(&window);
    window.arg1 = caller.lineEdit;
    window.arg2 = caller.lineEdit_2;
    window.result = caller.lineEdit_3;
    window.resize(240,320);

    window.show();
    return QApplication::exec();
}
