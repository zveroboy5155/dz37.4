

/*
This is a UI file (.ui.qml) that is intended to be edited in Qt Design Studio only.
It is supposed to be strictly declarative and only uses a subset of QML. If you edit
this file manually, you might introduce QML code that is not supported by Qt Design Studio.
Check out https://doc.qt.io/qtcreator/creator-quick-ui-forms.html for details on .ui.qml files.
*/
import QtQuick 2.15
//import QtQuick.Controls 2.15
//import QML_Proj
import QtQuick.Window 2.15
import QtQuick.Controls 6.3

Rectangle {
    id: rectangle

    //width: Constants.width
    //height: Constants.height
    color: Constants.backgroundColor
    transformOrigin: Item.Center
    layer.sourceRect.height: 50
    layer.sourceRect.width: 100
    focus: true
    //property alias columnX: column.x
    Column {
        id: column
        //width: 150
        //height: 200
        BorderImage {
            id: display
            //x: 270
            //y: 236
            width: 110
            height: 73
            source: "view.jpg"
            //source: "../view.jpg"
        }

        Row {
            id: row
            //x: 220
            //y: 263
            //width: 110
            //height: 30
            Label {
                //x: 231
                //y: 209
                id: label5
                text: "\u23EF"
            }

            Label {
                id: pause
                text: "\u23F8"
            }
            Label {
                id: stop
                text: "\u23F9"
            }
            Label {
                id: next
                text: "\u23E9"
            }
            Label {
                id: prev
                text: "\u23EA"
            }
        }
        ProgressBar {
            id: progressBar
            width: row.width
            height: row.height
            //width: 110
            //height: 15
            to: 100
            value: 60
        }
    }
}
