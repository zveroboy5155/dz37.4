#include <QMainWindow>
#include <QtWidgets/QLineEdit>
#ifndef DZ37_4_V2_MWINCL_H
#define DZ37_4_V2_MWINCL_H

class MwinnCall : public QMainWindow{
 Q_OBJECT
    int curVol = 0;
    int curCh = 0;

    void updateInfo(){
        result->setText("Channel:" +  QString::number(curCh) + "\nVolume:" + QString::number(curVol) + "%");
    }
public:
    QLineEdit *result = nullptr;
    MwinnCall(QWidget *parent = nullptr): QMainWindow(parent){};
public slots:
    void btn1(void ){
        curCh = 1;
        updateInfo();
    }

    void btn2(void ){
        curCh = 2;
        updateInfo();
    }

    void btn3(void ){
        curCh = 3;
        updateInfo();
    }

    void btn4(void ){
        curCh = 4;
        updateInfo();
    }

    void btn5(void ){
        curCh = 5;
        updateInfo();
    }

    void btn6(void ){
        curCh = 6;
        updateInfo();
    }

    void btn7(void ){
        curCh = 7;
        updateInfo();
    }

    void btn8(void ){
        curCh = 8;
        updateInfo();
    }

    void btn9(void ){
        curCh = 9;
        updateInfo();
    }

    void btn0(void ){
        curCh = 0;
        updateInfo();
    }

    void btnNextCh(){
        if(curCh >= 9)
            curCh -= 10;
        curCh++;
        updateInfo();
    }
    void btnPrevCh(){
        if(curCh <= 0)
            curCh += 10;
        curCh--;
        updateInfo();
    }
    void btnUpVol(){
        if(curVol == 100) return;
        curVol+= 10;
        updateInfo();
    }
    void btnDownVol(){
        if(curVol == 0) return;
        curVol-= 10;
        updateInfo();
    }
};
#endif //DZ37_4_V2_MWINCL_H
