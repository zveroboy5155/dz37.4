#include <QApplication>
#include <QPushButton>
#include "./MWinCl.h"
#include "./ui_DesQt1.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    MwinnCall window(nullptr);
    Ui::MainWindow caller;
    caller.setupUi(&window);
    window.result = caller.lineEdit;
    window.resize(240,680);

    window.show();
    return QApplication::exec();
}
